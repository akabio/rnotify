package rnotify_test

import (
	"log"
	"os"
	"path/filepath"
	"sort"
	"testing"
	"time"

	"gitlab.com/akabio/expect"
	"gitlab.com/akabio/rnotify"
)

type testNotify struct {
	w         *rnotify.Watcher
	t         *testing.T
	dir       string
	events    []rnotify.Event
	strEvents []string

	// options
	noInit   bool
	excludes []string
}

type testOpt interface {
	apply(*testNotify)
}

func tn(t *testing.T, options ...testOpt) *testNotify {
	dir, err := os.MkdirTemp("", "rnotify")
	if err != nil {
		t.Fatal(err)
	}

	tn := &testNotify{
		t:         t,
		dir:       dir,
		events:    []rnotify.Event{},
		strEvents: []string{},
	}
	for _, opt := range options {
		opt.apply(tn)
	}

	if !tn.noInit {
		tn.start()
	}

	return tn
}

func (tn *testNotify) start() {
	var err error
	tn.w, err = rnotify.New(tn.dir, tn.excludes)
	if err != nil {
		tn.t.Fatal(err)
	}

	go func() {
		for e := range tn.w.Events {
			tn.events = append(tn.events, e)
			tn.strEvents = append(tn.strEvents, e.String())
		}
	}()
}

func (tn *testNotify) writeFile(path string, ct string) {
	err := os.WriteFile(tn.path(path), []byte(ct), 0600)
	if err != nil {
		log.Fatal(err)
	}
}

func (tn *testNotify) rename(a string, b string) {
	err := os.Rename(tn.path(a), tn.path(b))
	if err != nil {
		tn.t.Fatal(err)
	}
}

func (tn *testNotify) appendFile(path string, ct string) {
	f, err := os.OpenFile(tn.path(path), os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		tn.t.Fatal(err)
	}

	if _, err = f.Write([]byte(ct)); err != nil {
		f.Close()
		tn.t.Fatal(err)
	}
	f.Close()
}

func (tn *testNotify) mkDir(path string) {
	err := os.MkdirAll(tn.path(path), 0700)
	if err != nil {
		log.Fatal(err)
	}
}

func (tn *testNotify) clear() {
	tn.sleep()
	tn.events = tn.events[:0]
	tn.strEvents = tn.strEvents[:0]
}

func (tn *testNotify) path(path string) string {
	return filepath.Join(tn.dir, path)
}

func (tn *testNotify) expectEvents() expect.Val {
	tn.t.Helper()

	tn.sleep()
	return expect.Value(tn.t, "events", tn.strEvents)
}

func (tn *testNotify) expectNormalizedEvents() expect.Val {
	tn.t.Helper()
	tn.sleep()

	dedup := map[string]bool{}
	for _, ev := range tn.strEvents {
		dedup[ev] = true
	}

	evts := []string{}
	for k := range dedup {
		evts = append(evts, k)
	}

	sort.Strings(evts)

	return expect.Value(tn.t, "events", evts)
}

func (tn *testNotify) sleep() {
	time.Sleep(time.Millisecond * 5)
}

func (tn *testNotify) close() {
	tn.w.Close()
	err := os.RemoveAll(tn.dir)
	if err != nil {
		tn.t.Fatal(err)
	}
}

// options

type noInitType struct {
}

func (ni *noInitType) apply(tn *testNotify) {
	tn.noInit = true
}

func noInit() testOpt {
	return &noInitType{}
}

type excludeType string

func (et excludeType) apply(tn *testNotify) {
	tn.excludes = append(tn.excludes, string(et))
}

func exclude(p string) testOpt {
	return excludeType(p)
}

func se(evts ...string) []string {
	return evts
}

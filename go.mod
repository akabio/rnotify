module gitlab.com/akabio/rnotify

go 1.17

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gobwas/glob v0.2.3
	gitlab.com/akabio/expect v0.9.6
)

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

package rnotify_test

import (
	"testing"
)

func TestCreateFileInRoot(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.writeFile("a.tst", "abc")
	tn.expectNormalizedEvents().ToBe(se("a.tst"))
}

func TestAppendFileInRoot(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.writeFile("a.tst", "abc")
	tn.clear()

	tn.appendFile("a.tst", "def")
	tn.expectEvents().ToBe(se("a.tst"))
}

func TestRenameFileInRoot(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.writeFile("a.tst", "abc")
	tn.sleep()

	tn.rename("a.tst", "b.tst")
	tn.expectNormalizedEvents().ToBe(se("a.tst", "b.tst"))
}

func TestCreateFolderInRoot(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.mkDir("folder")
	tn.expectEvents().ToCount(0)
}

func TestRenameFolderInRoot(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.mkDir("folder")

	tn.rename("folder", "older")
	tn.expectEvents().ToCount(0)
}

func TestCreateFileInExistingFolder(t *testing.T) {
	tn := tn(t, noInit())
	defer tn.close()

	tn.mkDir("folder")

	tn.start()

	tn.writeFile("folder/a.tst", "abc")
	tn.expectNormalizedEvents().ToBe(se("folder/a.tst"))
}

func TestCreateFileInNewFolder(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.mkDir("folder")
	tn.clear()

	tn.writeFile("folder/a.tst", "abc")
	tn.expectNormalizedEvents().ToBe(se("folder/a.tst"))
}

func TestCreateFileInRenamedFolder(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.mkDir("folder")
	tn.rename("folder", "older")

	tn.writeFile("older/a.tst", "abc")
	tn.expectNormalizedEvents().ToBe(se("older/a.tst"))
}

func TestCreateFileInDeepFolder(t *testing.T) {
	tn := tn(t)
	defer tn.close()

	tn.mkDir("folder/bolder/boulder/foo/bar")
	tn.writeFile("folder/bolder/boulder/foo/bar/b.tst", "abc")
	tn.sleep()
	tn.rename("folder", "older")

	tn.writeFile("older/bolder/boulder/foo/bar/a.tst", "abc")
	tn.expectNormalizedEvents().ToBe(se(
		"folder/bolder/boulder/foo/bar/b.tst",
		"older/bolder/boulder/foo/bar/a.tst",
		"older/bolder/boulder/foo/bar/b.tst",
	))
}

func TestCreateFileInExistingExcludesFolder(t *testing.T) {
	tn := tn(t, exclude("foo/bar/**"), noInit())
	defer tn.close()

	tn.mkDir("foo/bar/baz")

	tn.start()

	tn.writeFile("foo/bar/baz/x.tst", "zzz")
	tn.writeFile("foo/bar/y.tst", "zzz")

	tn.expectNormalizedEvents().ToBe(se("foo/bar/y.tst"))
}

func TestCreateFileInExcludesFolder(t *testing.T) {
	tn := tn(t, exclude("foo/bar/**"))
	defer tn.close()

	tn.mkDir("foo/bar/baz")
	tn.writeFile("foo/bar/baz/x.tst", "zzz")
	tn.writeFile("foo/bar/y.tst", "zzz")

	tn.expectNormalizedEvents().ToBe(se("foo/bar/y.tst"))
}

func TestCreateFileInRootExcludesFolder(t *testing.T) {
	tn := tn(t, exclude(".config"))
	defer tn.close()

	tn.mkDir(".config")
	tn.sleep()
	tn.mkDir(".config/foo")
	tn.sleep()
	tn.writeFile(".config/x.tst", "zzz")
	tn.writeFile(".config/foo/y.tst", "zzz")
	tn.writeFile("z.tst", "zzz")

	tn.expectNormalizedEvents().ToBe(se("z.tst"))
}

func TestCreateFileInSubExcludesFolder(t *testing.T) {
	tn := tn(t, exclude("**/build"))
	defer tn.close()

	tn.mkDir("projecta/build/sub")
	tn.sleep()
	tn.mkDir("projectb/build/sub")
	tn.mkDir("projectb/builder")
	tn.mkDir("projectb/src/pkg")
	tn.sleep()
	tn.writeFile("projecta/build/sub/lal.tst", "zzz")
	tn.writeFile("projectb/build/sub/other.tst", "zzz")
	tn.writeFile("projectb/builder/sh.tst", "zzz")
	tn.writeFile("projectb/src/pkg/code.tst", "zzz")

	tn.expectNormalizedEvents().ToBe(se("projectb/builder/sh.tst", "projectb/src/pkg/code.tst"))
}

func TestAssureExistingFilesDontBlockConstructor(t *testing.T) {
	tn := tn(t, noInit())
	defer tn.close()

	tn.mkDir("foo/bar/baz")
	tn.writeFile("foo/bar/baz/x.tst", "zzz")

	tn.start()

	tn.expectNormalizedEvents().ToBe(se("foo/bar/baz/x.tst"))
}

package rnotify

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/fsnotify/fsnotify"
	"github.com/gobwas/glob"
)

// Event notifies about a changed/created/moved file.
// It contains the path of the file relative to the root folder of the watcher.
type Event struct {
	Path string
}

func (e Event) String() string {
	return e.Path
}

// Watcher represents a watch from a folder recusrsively down the tree.
type Watcher struct {
	// Events is an unbuffered channel. It receives the filesystem changes as Event instances.
	// The channel will be closed when the watcher is closed with watcher.Close().
	Events chan (Event)

	watcher  *fsnotify.Watcher
	done     chan (int)
	root     string
	excludes []glob.Glob
}

// New creates a new watcher instance and starts listening for filechanges.
//
// The root parameter must be a path to the watched folder.
//
// The exclude array contains a list of folders that are recursively excluded from the watcher.
// So an exclude of ".git" will not listen for any changes in the .git folder and any subdirectories.
// The exclude matcher syntax allows paths in the form of `root/**/dist/asset*/ba[rz]/*.gen.ts`.
// see (gobwas)[https://pkg.go.dev/github.com/gobwas/glob#Compile] for more details.
func New(root string, exclude []string) (*Watcher, error) {
	excludes := []glob.Glob{}
	for _, ex := range exclude {
		gl, err := glob.Compile(ex, '/')
		if err != nil {
			return nil, err
		}
		excludes = append(excludes, gl)
	}

	fsw, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	w := &Watcher{
		watcher:  fsw,
		root:     filepath.Clean(root),
		done:     make(chan int),
		Events:   make(chan Event),
		excludes: excludes,
	}

	waitFor := make(chan bool)
	hasErr := make(chan error)

	go func() {
		// we add the root folder first and start processing events after.
		err = w.add(w.root)
		if err != nil {
			w.watcher.Close()
			hasErr <- err
		}
		waitFor <- true
		w.process()
	}()

	// wait till the watcher is registered
	// in case there are events during registration we read them so add doesn't block and then send them out after again.
	buffer := map[Event]bool{}
	for {
		select {
		case e := <-w.Events:
			buffer[e] = true
		case <-waitFor:
			// now we are sure the registration happended.
			// return and push out all the buffered events.
			go func() {
				for k := range buffer {
					w.Events <- k
				}
			}()
			return w, nil
		case err = <-hasErr:
			return nil, err
		}
	}
}

// Close unregisters the watcher and closes the Event channel.
func (w *Watcher) Close() {
	w.done <- 0
}

func (w *Watcher) addIgnoreNotFound(path string) error {
	err := w.add(path)
	if errors.Is(err, fs.ErrNotExist) {
		return nil
	}
	return err
}

func (w *Watcher) add(path string) error {
	relPath := w.relative(path)
	for _, ex := range w.excludes {
		if ex.Match(relPath) {
			return nil
		}
	}

	err := w.watcher.Add(path)
	if err != nil {
		return err
	}

	ls, err := os.ReadDir(path)
	if err != nil {
		return err
	}

	for _, l := range ls {
		if l.IsDir() {
			err = w.addIgnoreNotFound(filepath.Join(path, l.Name()))
			if err != nil {
				return err
			}
		} else {
			w.Events <- Event{Path: w.relative(filepath.Join(path, l.Name()))}
		}
	}

	return nil
}

func (w *Watcher) process() {
	go func() {
		for {
			select {
			case event, ok := <-w.watcher.Events:
				if !ok {
					close(w.Events)
					return
				}

				if event.Op&fsnotify.Create == fsnotify.Create {
					fi, err := os.Lstat(event.Name)
					if err != nil {
						// when renaming a folder it can happen that after the rename a CREATE
						// event is triggered with the old folder as name
						// we can just ignore those
						continue
					}
					if fi.IsDir() {
						// when traversing directories to add it can happen that a directory got deleted during traversing
						// so we ignore all not found errors from registering watches as well as iterating trough the subtree.
						err = w.addIgnoreNotFound(event.Name)
						if err != nil {
							log.Println(err)
						}
						// we don't wanna be informed about changed directories
						continue
					}
				}

				if event.Op&(fsnotify.Create|fsnotify.Write) > 0 {
					w.Events <- Event{Path: w.relative(event.Name)}
				}

			case err, ok := <-w.watcher.Errors:
				if !ok {
					close(w.Events)
					return
				}
				if err != nil {
					log.Println("error:", err)
				}
			case <-w.done:
				w.watcher.Close()
			}
		}
	}()
}

func (w *Watcher) relative(path string) string {
	if path == w.root {
		return ""
	}
	if strings.HasPrefix(path, w.root) {
		return path[len(w.root)+1:]
	}
	panic(fmt.Errorf("path is not absolute, %v", path))
}

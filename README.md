rnotify
=======

Watches a root folder and sends events whenever a file got changed in some way.

It uses fsnotify to do the heavy lifting.

rnotify has a very narrow focus. It only wants to be informed about file changes. Creating, writing, appending, renaming or discovering a file will all send the same notification.

It's recursive. It has one single root folder and it listens to all file changes inside. Some specific folders can be excluded. This will exclude all the sub tree of the matched folder.

problems
--------

It's not really reliable. When adding folders and files in fast sucession it can happen
that some events are missed because added watches take a while till they trigger events.

It can trigger extra events.

todo
----

- better error handling (channel?)
- testing on different os' (linux, osx, windows)
- stress testing
- ? throttle with deduplication of events

usage
-----

    import "gitlab.com/akabio/rnotify"

    ...

    rn, err := rnotify.New("folder", []string{".git", "**/node_modules"})
    if err != nil {
      ...
    }

    for evt := range rn.Events {
      fmt.Println(evt)
    }

apidoc
------
<https://pkg.go.dev/gitlab.com/akabio/rnotify>